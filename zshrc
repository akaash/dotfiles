#! /bin/zsh

ZSHC=~/.zshc
. $ZSHC/.zsh_config
. $ZSHC/.zsh_function
[ -s "$ZSHC/.zsh_work" ] && \. "$ZSHC/.zsh_work"
PATH="$(perl -e 'print join(":", grep { not $seen{$_}++ } split(/:/, $ENV{PATH}))')"
